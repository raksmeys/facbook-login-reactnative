/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import LoginTest from './src/components/LoginTest';

AppRegistry.registerComponent(appName, () => LoginTest);
