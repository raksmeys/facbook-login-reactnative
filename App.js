import React, { Component } from 'react'
import { SafeAreaView, Text, View } from 'react-native'
import { WebView } from 'react-native-webview'

export default class App extends Component {
  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {/* <WebView 
          originWhitelist={['*']}
          source={{uri: 'https://medium.com/@mehrankhandev/integrating-fbsdk-facebook-login-in-react-native-7b7600ce74a7'}}
        /> */}
        {/* <WebView 
          source={require('./src/assets/LocationTracking.pdf')}
        /> */}
        <WebView 
          source={{uri: 'https://www.youtube.com/watch?v=H7TjT2yZuDM&list=RDH7TjT2yZuDM&start_radio=1'}}
        />
      </SafeAreaView>
    )
  }
}
