import React, {Component} from 'react';
import {View, Text, Button, Image} from 'react-native';
import {
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';

export default class LoginTest extends Component {
  state = {userInfo: {}};

  getInfoFromToken = token => {
    const PROFILE_REQUEST_PARAMS = {
      fields: {
        string: 'id, name,  first_name, last_name, picture.type(large)',
      },
    };
    const profileRequest = new GraphRequest(
      '/me',
      {token, parameters: PROFILE_REQUEST_PARAMS},
      (error, result) => {
        if (error) {
          console.log('login info has error: ' + error);
        } else {
          this.setState({userInfo: result});
          console.log('result:', result);
        }
      },
    );
    new GraphRequestManager().addRequest(profileRequest).start();
  };

  render() {
    return (
      <View style={{flex: 1, margin: 50}}>
        <LoginButton
          onLoginFinished={(error, result) => {
            alert('start login')
            if (error) {
              console.log('login has error: ' + result.error);
            } else if (result.isCancelled) {
              console.log('login is cancelled.');
            } else {
              AccessToken.getCurrentAccessToken().then(data => {
                const accessToken = data.accessToken.toString();
                alert(accessToken)
                this.getInfoFromToken(accessToken);
              });
            }
          }}
          onLogoutFinished={() => this.setState({userInfo: {}})}
        />
        {this.state.userInfo.name && (
          <Text style={{fontSize: 16, marginVertical: 16}}>
            ID:  {this.state.userInfo.id}
            NAME: {this.state.userInfo.name}
            {/* FIRST NAME: {this.state.userInfo} */}
          </Text>
        )}
        <Text>Helo {this.state.userInfo.name}</Text>
        
        {
          this.state.userInfo.picture !== undefined && <Image 
          style={{width: 100, height: 100, borderRadius: 50}}
          source={{uri: this.state.userInfo.picture.data.url}}
        />
        }

      </View>
    );
  }
}